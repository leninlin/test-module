package com.decurtis.couchbase;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.telecom.Call;
import android.util.Base64;
import android.widget.Toast;

import com.couchbase.lite.ArrayExpression;
import com.couchbase.lite.ArrayFunction;
import com.couchbase.lite.Authenticator;
import com.couchbase.lite.BasicAuthenticator;
import com.couchbase.lite.Blob;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.DataSource;
import com.couchbase.lite.Database;
import com.couchbase.lite.DatabaseConfiguration;
import com.couchbase.lite.Dictionary;
import com.couchbase.lite.Document;
import com.couchbase.lite.DocumentFlag;
import com.couchbase.lite.DocumentReplication;
import com.couchbase.lite.DocumentReplicationListener;
import com.couchbase.lite.Endpoint;
import com.couchbase.lite.Expression;
import com.couchbase.lite.FullTextExpression;
import com.couchbase.lite.Function;
import com.couchbase.lite.Join;
import com.couchbase.lite.ListenerToken;
import com.couchbase.lite.Meta;
import com.couchbase.lite.MutableArray;
import com.couchbase.lite.MutableDocument;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryBuilder;
import com.couchbase.lite.ReplicatedDocument;
import com.couchbase.lite.Replicator;
import com.couchbase.lite.ReplicatorChange;
import com.couchbase.lite.ReplicatorChangeListener;
import com.couchbase.lite.ReplicatorConfiguration;
import com.couchbase.lite.Result;
import com.couchbase.lite.ResultSet;
import com.couchbase.lite.SelectResult;
import com.couchbase.lite.URLEndpoint;
import com.couchbase.lite.internal.support.Log;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CouchbaseLite extends ReactContextBaseJavaModule implements ReplicatorChangeListener {

    private final ReactContext mReactContext;
    public static final String TAG = "CouchbaseLite";

    private Database database = null;
    private Replicator replicator;
    private ReplicatorConfiguration config = null;
    private ListenerToken listenerToken = null;

    private String syncGateway = null;
    private String username = null;
    private String password = null;

    CouchbaseLite(ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;
    }

    @Override
    public String getName() {
        return "CouchbaseLite";
    }

    @ReactMethod
    public void queryDocument(String docType, int count, int offset, Promise promise) {
        Query query = null;
        if (count > 0 && docType != null) {
            query = QueryBuilder
                    .select(SelectResult.all(),SelectResult.expression(Meta.id))
                    .from(DataSource.database(database))
                    .where(Expression.property("type").equalTo(Expression.string(docType)))
                    .limit(Expression.intValue(count), Expression.intValue(offset));
        }
        if (count == 0 && docType != null) {
            query = QueryBuilder
                    .select(SelectResult.all(),SelectResult.expression(Meta.id))
                    .from(DataSource.database(database))
                    .where(Expression.property("type").equalTo(Expression.string(docType)));
        }
        try {
            ResultSet rs = query.execute();
            WritableArray writableArray = Arguments.createArray();
            for (Result result : rs) {
                WritableMap writableMap = Arguments.makeNativeMap(result.toMap());
                writableArray.pushMap(writableMap);
            }
            promise.resolve(writableArray);
        } catch (CouchbaseLiteException e) {
            promise.reject("queryDocument",e.getMessage());
        }
    }

    @ReactMethod
    public void queryDocumentValues(String docType, String property, ReadableArray values, Boolean strict, Promise promise) {
        Query query = null;
        if (docType != null && values != null) {
            Expression typeExpr = Expression.property("type").equalTo(Expression.string(docType));

            Expression propExpr;
            if(strict){
                propExpr = Expression.property(property).like(Expression.string(values.getString(0)));
                for(int i=1; i < values.size(); i++){
                    propExpr = propExpr.or(Expression.property(property).like(Expression.string(values.getString(i))));
                }
            } else {
                propExpr = Function.lower(Expression.property(property)).like(Expression.string(values.getString(0).toLowerCase()));
                for(int i=1; i < values.size(); i++){
                    propExpr = propExpr.or(Function.lower(Expression.property(property)).like(Expression.string(values.getString(i).toLowerCase())));
                }
            }

            query = QueryBuilder
                    .select(SelectResult.all(),SelectResult.expression(Meta.id))
                    .from(DataSource.database(database))
                    .where(typeExpr.and(propExpr));
        }
        try {
            ResultSet rs = query.execute();
            WritableArray writableArray = Arguments.createArray();
            for (Result result : rs) {
                WritableMap writableMap = Arguments.makeNativeMap(result.toMap());
                writableArray.pushMap(writableMap);
            }
            promise.resolve(writableArray);
        } catch (CouchbaseLiteException e) {
            promise.reject("queryDocument",e.getMessage());
        }
    }

    @ReactMethod
    public void queryDocumentProps(String docType, ReadableArray properties, String value, Boolean strict, Promise promise) {
        Query query = null;
        if (docType != null && properties != null) {
            Expression typeExpr = Expression.property("type").equalTo(Expression.string(docType));

            Expression propExpr;
            if(strict){
                propExpr = Expression.property(properties.getString(0)).like(Expression.string(value));
                for(int i=1; i < properties.size(); i++){
                    propExpr = propExpr.or(Expression.property(properties.getString(i)).like(Expression.string(value)));
                }
            } else {
                propExpr = Function.lower(Expression.property(properties.getString(0))).like(Expression.string(value.toLowerCase()));
                for(int i=1; i < properties.size(); i++){
                    propExpr = propExpr.or(Function.lower(Expression.property(properties.getString(i))).like(Expression.string(value.toLowerCase())));
                }
            }
            query = QueryBuilder
                    .select(SelectResult.all(),SelectResult.expression(Meta.id))
                    .from(DataSource.database(database))
                    .where(typeExpr.and(propExpr));
        }
        try {
            ResultSet rs = query.execute();
            WritableArray writableArray = Arguments.createArray();
            for (Result result : rs) {
                WritableMap writableMap = Arguments.makeNativeMap(result.toMap());
                writableArray.pushMap(writableMap);
            }
            promise.resolve(writableArray);
        } catch (CouchbaseLiteException e) {
            promise.reject("queryDocument",e.getMessage());
        }
    }

    @ReactMethod
    public void getDocumentCount(String docType, Promise promise) {
        if (docType != null) {
            Query query = QueryBuilder
                    .select(SelectResult.expression(Function.count(Expression.all())))
                    .from(DataSource.database(database)).where(Expression.property("type").equalTo(Expression.string(docType)));
            try {
                ResultSet rs = query.execute();
                for (Result result : rs) {
                    WritableMap writableMap = Arguments.makeNativeMap(result.toMap());
                    promise.resolve(String.format("%.0f",writableMap.getDouble("$1")));
                    return;
                }
            } catch (CouchbaseLiteException e) {
                promise.reject("getDocumentCount", e.getMessage());
            }
        } else {
            promise.resolve(false);
        }
    }

    @ReactMethod
    public void queryGreaterThanTimeAgoForIds(String docType, String property, int minutes, Promise promise) {
        if (property != null) {
            Date dateAgo = new Date(System.currentTimeMillis() - minutes * 60 * 1000);
            Expression typeExpr = Expression.property("type").equalTo(Expression.string(docType));
            Expression timeExpr = Expression.property(property).greaterThanOrEqualTo(Expression.date(dateAgo));
            Query query = QueryBuilder
                        .select(SelectResult.all(), SelectResult.expression(Meta.id))
                        .from(DataSource.database(database))
                        .where(typeExpr.and(timeExpr));

            try {
                ResultSet rs = query.execute();
                WritableArray writableArray = Arguments.createArray();
                for (Result result : rs) {
                    WritableMap writableMap = Arguments.makeNativeMap(result.toMap());
                    writableArray.pushMap(writableMap);
                }
                promise.resolve(writableArray);
            } catch (CouchbaseLiteException e) {
                promise.reject("queryGreaterThanTimeAgoForIds", e.getMessage());
            }
        }
        else {
            promise.resolve(false);
        }
    }

    @ReactMethod
    public void getDocument(String docID, String key, Promise promise) {
        Document doc = this.database.getDocument(docID);
        if (doc == null) {
            promise.resolve(false);
        } else {
            if(key == null) {
                WritableMap writableMap = Arguments.makeNativeMap(doc.toMap());
                promise.resolve(writableMap);
            }
            else {
                Object value = doc.getValue(key);
                if(value != null){
                    promise.resolve(value.toString());
                }
                promise.resolve(value);
            }
        }
    }

    @ReactMethod
    public void addDocument(String docID, ReadableMap object, Promise promise) {
        MutableDocument mutableDocument = null;
        if(docID != null){
            mutableDocument = new MutableDocument(docID, object.toHashMap());
        } else {
            mutableDocument = new MutableDocument(object.toHashMap());
        }

        try{
            this.database.save(mutableDocument);
            if (mutableDocument == null) {
                promise.resolve(false);
            } else {
                this.database.save(mutableDocument);

                WritableMap result = Arguments.makeNativeMap(mutableDocument.toMap());
                result.putString("id", mutableDocument.getId());
                promise.resolve(result);
            }
        }catch (CouchbaseLiteException e) {
            promise.reject("addDocument",e.getMessage());
        }
    }

    @ReactMethod
    public void editDocument(String docID, ReadableMap object, Promise promise) {
        Document doc = this.database.getDocument(docID);
        MutableDocument mutableDocument = doc.toMutable();
        if (doc == null) {
            promise.resolve(false);
            return;
        }
        try {
            HashMap<String,Object> objectHashMap = object.toHashMap();
            for (Map.Entry<String, Object> entry : objectHashMap.entrySet()) {
                mutableDocument.setValue(entry.getKey(),entry.getValue());
            }

            this.database.save(mutableDocument);
            
            WritableMap result = Arguments.makeNativeMap(mutableDocument.toMap());
            result.putString("id", mutableDocument.getId());
            promise.resolve(result);
        } catch (Exception e) {
            promise.reject("editDocument", "Can not edit document", e);
        }
    }

    @ReactMethod
    public void deleteDocument(String docID, Promise promise) {
        try {
            Document doc = this.database.getDocument(docID);
            this.database.delete(doc);

            promise.resolve(true);
        } catch (Exception e) {
            promise.reject("deleteDocument", "Can not delete document", e);
        }
    }

    @ReactMethod
    public void addFileAttachment(String docID, String contentUri, String attachmentName, Promise promise) {
        try {
            Uri uri = Uri.parse(contentUri);
            InputStream stream = this.mReactContext.getContentResolver().openInputStream(uri);
            String contentType = this.mReactContext.getContentResolver().getType(uri);

            Document doc = this.database.getDocument(docID);
            MutableDocument mutableDocument = doc.toMutable();
            Blob blob = new Blob(contentType, stream);
            mutableDocument.setBlob(attachmentName, blob);
            this.database.save(mutableDocument);

            WritableMap result = Arguments.createMap();
            result.putString("attachmentName",attachmentName);
            result.putBoolean("result", true);
            promise.resolve(result);
        } catch (Exception e) {
            promise.reject("addAttachment", "Can not add attachment", e);
        }
    }

    @ReactMethod
    public void addBase64Attachment(String docID, String attachmentName, String contentType, String base64, Promise promise) {
        InputStream is = new ByteArrayInputStream(Base64.decode(base64.getBytes(), Base64.DEFAULT));
        try {
            Document doc = this.database.getDocument(docID);
            if(doc != null) {
                MutableDocument mutableDocument = doc.toMutable();
                Blob blob = new Blob(contentType, is);
                mutableDocument.setBlob(attachmentName, blob);
                database.save(mutableDocument);
                promise.resolve(true);
            } else {
                promise.resolve(false);
            }
        } catch (CouchbaseLiteException e) {
            promise.resolve(false);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                promise.resolve(false);
            }
        }
    }

    @ReactMethod
    public void getBase64Attachment(String docID, String filename, Promise promise) {
        Document doc = this.database.getDocument(docID);
        if(doc != null) {
            MutableDocument mutableDocument = doc.toMutable();
            Blob taskBlob = mutableDocument.getBlob(filename);

            byte[] imageBytes = taskBlob.getContent();
            String imageStr = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            promise.resolve(imageStr);
        } else {
            promise.resolve(false);
        }
    }

    @ReactMethod
    public void deleteProperty(String docID, String property, Promise promise) {
        try {
            Document doc = this.database.getDocument(docID);
            if(doc != null) {
                MutableDocument mutableDocument = doc.toMutable();
                mutableDocument. remove(property);
                this.database.save(mutableDocument);

                WritableMap result = Arguments.createMap();
                result.putString("property",property);
                result.putBoolean("result", true);
                promise.resolve(result);
            } else {
                promise.resolve(false);
            }
        } catch (Exception e) {
            promise.reject("deleteProperty", "Can not delete property", e);
        }
    }

    // -------------------------
    // Database operation
    // -------------------------

    @ReactMethod
    private void setSyncGateway(String _syncGateway, Promise promise){
        if(_syncGateway != null) {
            syncGateway = _syncGateway;
            promise.resolve(true);
        }
        else{
            promise.reject("setSyncGateway", "setSyncGateway is null");
        }
    }

    @ReactMethod
    private void setUsernamePassword(String _username, String _password, Promise promise){
        if(_username != null && _password != null) {
            username = _username;
            password = _password;
            promise.resolve(true);
        }
        else{
            promise.reject("setUsernamePassword", "Username or password is null");
        }
    }

    @ReactMethod
    private void openDatabase(String dbName, Promise promise) {
        DatabaseConfiguration config = new DatabaseConfiguration(getApplicationContext());
        try {
            database = new Database(dbName, config);
            promise.resolve(true);
        } catch (CouchbaseLiteException e) {
            promise.reject("openDatabase",e.getMessage());
        }
    }

    private void closeDatabase(Promise promise) {
        if (database != null) {
            try {
                database.close();
                database = null;
                promise.resolve(true);
            } catch (CouchbaseLiteException e) {
                promise.reject("closeDatabase",e.getMessage());
            }
        }
    }

    // -------------------------
    // Replications
    // -------------------------

    @ReactMethod
    private void initReplication(Promise promise) {
        URI uri;
        try {
            uri = new URI(syncGateway);
        } catch (URISyntaxException e) {
            promise.reject("initReplication", e.getMessage());
            return;
        }

        Endpoint endpoint = new URLEndpoint(uri);
        config = new ReplicatorConfiguration(database, endpoint)
                .setReplicatorType(ReplicatorConfiguration.ReplicatorType.PUSH_AND_PULL)
                .setContinuous(true);

        // authentication
        if (username != null && password != null)
            config.setAuthenticator(new BasicAuthenticator(username, password));

        promise.resolve(true);
    }

    @ReactMethod
    private void setChannels(ReadableArray array, Promise promise) {
        if (config == null) {
            promise.reject("setChannels", "Config is null");
            return;
        }
        List<String> channels = new ArrayList<String>();
        for(int i=0; i < array.size(); i++){
            channels.add(array.getString(i));
        }
        config.setChannels(channels);
        promise.resolve(true);
    }

    @ReactMethod
    private void setDocumentIDs(ReadableArray array, Promise promise) {
        if (config == null) {
            promise.reject("setDocumentIDs", "Config is null");
            return;
        }
        List<String> docs = new ArrayList<String>();
        for(int i=0; i < array.size(); i++){
            docs.add(array.getString(i));
        }
        config.setDocumentIDs(docs);
        promise.resolve(true);
    }

    @ReactMethod
    private void startReplication(Promise promise) {
        if(replicator != null ) {
            replicator.stop();
            replicator = null;
        }

        replicator = new Replicator(config);
        replicator.addChangeListener(this);
        listenerToken = replicator.addDocumentReplicationListener(new DocumentReplicationListener() {
            @Override
            public void replication(@NonNull DocumentReplication replication) {
                WritableArray wa = Arguments.createArray();

                WritableMap wType = Arguments.createMap();
                wType.putString("ReplicationType", ((replication.isPush()) ? "Push" : "Pull"));
                wa.pushMap(wType);

                for (ReplicatedDocument document : replication.getDocuments()) {
                    WritableMap wDoc = Arguments.createMap();
                    wDoc.putString("docID", document.getID());

                    CouchbaseLiteException err = document.getError();
                    if (err != null) {
                        wDoc.putString("error: ", err.toString());
                    }
                    if (document.flags().contains(DocumentFlag.DocumentFlagsDeleted)) {
                        wDoc.putString("documentFlag", "Successfully replicated a deleted document");
                    }
                    if (document.flags().contains(DocumentFlag.DocumentFlagsAccessRemoved)) {
                        wDoc.putString("documentFlag", "Successfully purged a deleted document");
                    }
                    wa.pushMap(wDoc);
                }
                CouchbaseLite.this.sendEvent("ReplicatorDocumentChangeListener", wa);
            }
        });
        replicator.start();
        promise.resolve(true);
    }

    @ReactMethod
    private void stopReplication(Promise promise) {
        replicator.stop();
        replicator.removeChangeListener(listenerToken);
        promise.resolve(true);
    }

    @ReactMethod
    private void getStatusOfReplication(Promise promise) {
        if (replicator == null) {
            promise.reject("getStatusOfReplication", "Replicator is null");
            return;
        }
        WritableMap wm = Arguments.createMap();
        wm.putString("getProgress",replicator.getStatus().getProgress().toString());
        wm.putString("getActivityLevel",replicator.getStatus().getActivityLevel().toString());
        promise.resolve(wm);
    }

    // --------------------------------------------------
    // ReplicatorChangeListener implementation
    // --------------------------------------------------
    @Override
    public void changed(ReplicatorChange change) {
        WritableMap wm = Arguments.createMap();
        wm.putString("getProgress",change.getStatus().getProgress().toString());
        wm.putString("getActivityLevel",change.getStatus().getActivityLevel().toString());
        this.sendEvent("ReplicatorChangeListener", wm);
    }

    private void sendEvent(String eventName,@Nullable Object params) {
        mReactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
    }
}
