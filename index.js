import { NativeModules, DeviceEventEmitter, NativeEventEmitter, Platform } from 'react-native'
const { CouchbaseLite } = NativeModules

export const couchbaseConstants = {
  cacheStorage: 'cacheStorage'
}
const CouchbaseEvents = new NativeEventEmitter(CouchbaseLite)

/**
 * Class to use native database and synchronize it with server
 */
export default class Couchbase {
  /**
   * Set database name, username and password for access to bucket, bucket's url, channels to replicate
   * @param {Object} param0
   */
  constructor ({ dbName, username, password, syncGateway, channels }) {
    if (username && password) {
      this.setUsernamePassword(username, password)
    }
    if (dbName) {
      this.openDatabase(dbName)
      if (!this.getDocument(couchbaseConstants.cacheStorage, '')) {
        this.addDocument(couchbaseConstants.cacheStorage, { type: couchbaseConstants.cacheStorage })
      }
    }
    if (syncGateway) {
      this.setSyncGateway(syncGateway)
      this.initReplication()
    }
    if (channels) {
      this.setChannels(channels)
    }

    /**
     * getActivityLevel:
     * STOPPED: The replication is finished or hit a fatal error.
     * OFFLINE: The replicator is offline as the remote host is unreachable.
     * CONNECTING: The replicator is connecting to the remote host.
     * IDLE: The replication caught up with all the changes available from the server. The IDLE state is only used in continuous replications.
     * BUSY: The replication is actively transferring data.
     */
    this.lastStatusReplication = {
      getActivityLevel: null,
      getProgress: null
    }
  }

  /**
   * Set username and password for access to bucket (need before initReplication)
   * @param {String} username
   * @param {String} password
   */
  setUsernamePassword (username, password) {
    return CouchbaseLite.setUsernamePassword(username, password)
  }

  /**
   * Create database with given name (need before initReplication)
   * @param {String} databaseName
   */
  openDatabase (databaseName) {
    return CouchbaseLite.openDatabase(databaseName)
  }

  /**
   * Close opened database
   */
  closeDatabase () {
    return CouchbaseLite.closeDatabase()
  }

  /**
   * Set bucket url (need before initReplication)
   * @param {String} url
   */
  setSyncGateway (url) {
    return CouchbaseLite.setSyncGateway(url)
  }

  /**
   * Initialize params to next replication
   */
  initReplication () {
    return CouchbaseLite.initReplication()
  }

  /**
   * Start replication with given params
   */
  startReplication () {
    return CouchbaseLite.startReplication()
  }

  /**
   * Get status of replication (if replication was started)
   */
  async getStatusOfReplication () {
    const status = await CouchbaseLite.getStatusOfReplication()
    this.lastStatusReplication = status
    return status
  }

  /**
   * Stop current replication
   */
  stopReplication () {
    return CouchbaseLite.stopReplication()
  }

  /**
   * Set array of channels which need to replicate
   * @param {Array<String>} channels
   */
  setChannels (channels) {
    return CouchbaseLite.setChannels(channels)
  }

  /**
   * Set array of document's ID which need to replicate
   * @param {Array<String>} documents
   */
  setDocumentIDs (documents) {
    return CouchbaseLite.setDocumentIDs(documents)
  }

  /**
   * Get docuement with ID and key (optional)
   * @param {String} docID
   */
  getDocument (docID, key) {
    return CouchbaseLite.getDocument(docID, key)
  }

  /**
   * Query documents with type, with count and offset
   * @param {String} docType
   * @param {Number} count
   * @param {Number} offset
   */
  queryDocument (docType, count, offset) {
    return CouchbaseLite.queryDocument(docType, count, offset)
  }

  /**
   * Query documents with type, array of properties and single value. Can be strictable
   * @param {String} docType
   * @param {Array<String>} properties
   * @param {String} value
   * @param {Boolean} strict
   */
  queryDocumentProps (docType, properties, value, strict) {
    return CouchbaseLite.queryDocumentProps(docType, properties, value, strict)
  }

  /**
   * Query documents with type, single property and array of values. Can be strictable
   * @param {String} docType
   * @param {String} property
   * @param {Array<String>} values
   * @param {Boolean} strict
   */
  queryDocumentValues (docType, property, values, strict) {
    return CouchbaseLite.queryDocumentValues(docType, property, values, strict)
  }

  /**
   * Query documents with property (Date), greater than, or equal to %NOWDATE% - minutes
   * @param {String} docType
   * @param {String} property
   * @param {Number} minutes
   */
  queryGreaterThanTimeAgoForIds (docType, property, minutes) {
    return CouchbaseLite.queryGreaterThanTimeAgoForIds(docType, property, minutes)
  }

  /**
   * Get count of document with type - docType
   * @param {String} docType
   */
  getDocumentCount (docType) {
    return CouchbaseLite.getDocumentCount(docType)
  }

  /**
   * Add document to database. Returns added document
   * @param {String} docID
   * @param {Object} document
   */
  addDocument (docID, document) {
    return CouchbaseLite.addDocument(docID, document)
  }

  /**
   * Edit document from database with ID, if exist property will be rewrited. Returns edited document
   * @param {String} docID
   * @param {Object} document
   */
  editDocument (docID, document) {
    return CouchbaseLite.editDocument(docID, document)
  }

  /**
   * Delete property from document (can be blob)
   * @param {String} docID
   * @param {String} property
   */
  deleteProperty (docID, property) {
    return CouchbaseLite.deleteProperty(docID, property)
  }

  /**
   * Delete document from database with ID
   * @param {String} docID
   */
  deleteDocument (docID) {
    return CouchbaseLite.deleteDocument(docID)
  }

  /**
   * Add attachment to document with ID, from path - contentUri
   * @param {String} docID
   * @param {String} contentUri
   * @param {String} attachmentName
   */
  addFileAttachment (docID, contentUri, attachmentName) {
    return CouchbaseLite.addFileAttachment(docID, contentUri, attachmentName)
  }

  /**
   * Add attachment to document with ID, from base64 (with MIME)
   * @param {String} docID
   * @param {String} filename
   * @param {String} contentType
   * @param {String} base64
   */
  addBase64Attachment (docID, filename, contentType, base64) {
    return CouchbaseLite.addBase64Attachment(docID, filename, contentType, base64)
  }

  /**
   * Get base64 string from document by attachmentName
   * @param {String} docID
   * @param {String} attachmentName
   */
  getBase64Attachment (docID, attachmentName) {
    return CouchbaseLite.getBase64Attachment(docID, attachmentName)
  }

  /**
   * Function to show status of replication
   * @param {Object} status
   */
  handleReplicationStatus (status, listenerType) {
    this.lastStatusReplication = status
    // console.log(listenerType, status)
  }

  /**
   * Add listner of replication status
   */
  addReplicatorChangeListener () {
    const listenerType = 'ReplicatorChangeListener'
    if (Platform.OS === 'android') {
      DeviceEventEmitter.addListener(listenerType, (status) => this.handleReplicationStatus(status, listenerType))
    } else if (Platform.OS === 'ios') {
      CouchbaseEvents.addListener(listenerType, (status) => this.handleReplicationStatus(status, listenerType))
    }
  }

  /**
   * Remove listner of replication status
   */
  removeReplicatorChangeListener () {
    const listenerType = 'ReplicatorChangeListener'
    if (Platform.OS === 'android') {
      DeviceEventEmitter.removeListener(listenerType, this.handleReplicationStatus.bind(this))
    } else if (Platform.OS === 'ios') {
      CouchbaseEvents.removeListener(listenerType, this.handleReplicationStatus.bind(this))
    }
  }

  /**
   * Add listner of document's replication status
   */
  addReplicatorDocumentChangeListener () {
    const listenerType = 'ReplicatorDocumentChangeListener'
    if (Platform.OS === 'android') {
      DeviceEventEmitter.addListener(listenerType, (status) => this.handleReplicationStatus(status, listenerType))
    } else if (Platform.OS === 'ios') {
      CouchbaseEvents.addListener(listenerType, (status) => this.handleReplicationStatus(status, listenerType))
    }
  }

  /**
   * Remove listner of document's replication status
   */
  removeReplicatorDocumentChangeListener () {
    const listenerType = 'ReplicatorDocumentChangeListener'
    if (Platform.OS === 'android') {
      DeviceEventEmitter.removeListener(listenerType, this.handleReplicationStatus.bind(this))
    } else if (Platform.OS === 'ios') {
      CouchbaseEvents.removeListener(listenerType, this.handleReplicationStatus.bind(this))
    }
  }

  /** CUSTOM FUNCTIONS **/

  async putToCache (key, value) {
    if (key) {
      let cacheObject = {}
      cacheObject[key] = value
      await this.editDocument(couchbaseConstants.cacheStorage, cacheObject)
    }
  }

  async getFromCache (key) {
    if (key) {
      let cacheObject = {}
      cacheObject[key] = await this.getDocument(couchbaseConstants.cacheStorage, key)
      return cacheObject
    }
  }
}
